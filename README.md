> Задание 1&2 

```
git clone https://gitlab.com/xhilatreae/test-task.git
cd test-task/
pip install -r requirements.txt
python manage.py createsuperuser
python manage.py runserver
```




> Задание 3 
```
INSERT INTO таблица4 (InternalNumber, `Name/Surname`, Position, `Salary/Month`, Tax, Month)
SELECT
    pos.InternalNumber,
    (SELECT CONCAT_WS(" ", Name, Surname)) "Name/Surname",
    pos.Position,
    (SELECT emp.`Salary/year`/12) "Salary/Month",
    tax.Taxes,
    tax.Month
 FROM
	таблица2 tax JOIN таблица3 pos ON tax.ID = pos.ID
    JOIN таблица1 emp ON emp.ID = tax.ID
    ORDER BY emp.Name
```

    
> Вывод содержимого полученной таблицы:

``select * from таблица4``

