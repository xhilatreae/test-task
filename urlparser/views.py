from django.shortcuts import render
from django.http import HttpResponse
from .models import Urls
# Create your views here.

def index(request):
    return render(request, 'urlparser/index.html', {'test': Urls.objects.all()})
