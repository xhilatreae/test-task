# Generated by Django 2.2.4 on 2019-09-12 10:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('urlparser', '0002_auto_20190912_1430'),
    ]

    operations = [
        migrations.AddField(
            model_name='urls',
            name='parsed',
            field=models.BooleanField(default=False),
        ),
    ]
