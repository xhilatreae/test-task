from django.urls import path
from . import views
from .models import Urls
from bs4 import BeautifulSoup
from threading import Timer
from datetime import datetime
import requests
urlpatterns = [
	path('', views.index, name='index'),
]

def parse(vals):
    obj = Urls.objects.get(address = vals.address)
    obj.date = datetime.now()
    obj.isParsed = True

    try:
        soup = BeautifulSoup(requests.get(vals.address).text)
    except:
        obj.save()
        return
   
    obj.title = str(soup.find('title'))
    obj.headers = str(soup.find('h1'))
    obj.isSucceed = True
    obj.charset = str(soup.meta.get('charset'))
    obj.save()

def startup():
    for vals in Urls.objects.all():
        if vals.isParsed == False or vals.isSucceed == False:
            if int(vals.timeshift) > 0 :
                t = Timer(int(vals.timeshift), parse, args=(vals,)).start()
            else:
                #t = Timer(0, parse, args=(vals,)).start()
                parse(vals)

startup()