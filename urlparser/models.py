from django.db import models


# Create your models here.

class Urls(models.Model):
    address = models.CharField(max_length = 150)
    timeshift = models.IntegerField(default = 0, blank=True)
    date = models.DateTimeField(blank = True, null = True)
    charset = models.CharField(max_length = 10, blank=True)
    title = models.CharField(max_length = 150, blank=True)
    headers = models.TextField(default = '', blank=True)
    isParsed = models.BooleanField(default = False)
    isSucceed = models.BooleanField(default = False)