from channels.generic.websocket import WebsocketConsumer
from .models import Urls
from django.core import serializers
import json

class DataConsumer(WebsocketConsumer):
	def connect(self):
		self.accept()

	def disconnect(self, close_code):
		pass

	def receive(self, text_data):
		i = 0
		x = serializers.serialize('json', Urls.objects.all())
		data = json.loads(x)
		address = []
		date = []
		title = []
		headers = []
		charsets = []
		isParsed = []
		isSucceed = []
		while i < len(data):
			address.append(data[i]['fields']["address"])
			date.append(data[i]['fields']["date"])
			title.append(data[i]['fields']["title"])
			headers.append(data[i]['fields']["headers"])
			charsets.append(data[i]['fields']['charset'])
			isParsed.append(data[i]['fields']['isParsed'])
			isSucceed.append(data[i]['fields']['isSucceed'])
			i+=1
		self.send(text_data = json.dumps({
			'urls' : address,
			'dates': date,
			'titles': title,
			'headers': headers,
			'charsets': charsets,
			'isParsed': isParsed,
			'isSucceed': isSucceed
		}))